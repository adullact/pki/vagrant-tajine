# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)


--------------------------------

## v2.6.0, 2024.09.11

### Added

- feat(make): add `clean_all` command

### Changed

- feat: use [Tajine `v1.1.5`](https://gitlab.adullact.net/adullact/pki/tajine/-/releases/v1.1.5)
- fix: use the new puppet module configuration (see: `adullact/puppet-tajine` **4.0.0**)

### Dependencies

- use [adullact/puppet-tajine `4.0.0`](https://gitlab.adullact.net/adullact/puppet-tajine/-/releases/4.0.0)  instead of `3.0.0`


--------------------------------

## v2.5.0, 2024.07.11

### Added

- feat: add Makefile

### Changed

- feat: use [Tajine `v1.1.2`](https://gitlab.adullact.net/adullact/pki/tajine/-/releases/v1.1.2)
- chore: use puppet-tajine `3.0.0` #4
- chore: use puppet-cfssl `3.0.1` #5
- chore: update all other Puppet modules (apache, apt, concat, postgresql, stdlib, vcsrepo, archive and systemd) #6


--------------------------------

## v2.4.2, 2024.06.05

### Changed

- feat: use [Tajine `v1.1.1`](https://gitlab.adullact.net/adullact/pki/tajine/-/releases/v1.1.1)


--------------------------------

## v2.4.1, 2024.05.24

### Changed

- feat: use [Tajine `v1.1.0`](https://gitlab.adullact.net/adullact/pki/tajine/-/releases/v1.1.0)


--------------------------------

## v2.4.0, 2023.11.09

### Added

- docs(readme): add shared folders Host <--> Guest (VM)

### Changed

- feat: use [Tajine `v1.0.0`](https://gitlab.adullact.net/adullact/pki/tajine/-/releases/v1.0.0)
- feat: set **fr** to i18n default locale parameter
- chore: use puppet-cfssl `2.1.1`
- chore: use puppet-tajine `2.0.0`

### Fixed

- fix: disable forced_ssl option of Symfony component nelmio_security
- docs(readme): docs: improve titles


--------------------------------

## v2.3.0, 2023.10.03

### Added

- feat(vagrant): add shared folders Host <--> Guest (VM)

### Changed

- feat: use [Tajine `v0.14.0`](https://gitlab.adullact.net/adullact/pki/tajine/-/releases/v0.14.0)
- chore: use puppet-tajine 1.1.1


--------------------------------

## v2.2.2, 2023.06.19

### Fixed

- fix(puppet): enable Apache headers module


--------------------------------

## v2.2.1, 2023.06.14

### Changed

- Use Tajine v0.9.3


--------------------------------

## v2.2.0, 2023.05.31

### Added

- feat: allow access to CFSSL and Tajine databases from an IDE
- docs: warning ("Created **only** for **test** or **demonstration** purposes.")
- docs: add contributing file

### Changed

- fix(tajine): disabled `DEBUG_CERT_BY_ADMIN` environment variable
- fix(vagrant provision): use puppet7
- refactor: improve puppet manifest

--------------------------------


## v2.1.0, 2023.05.30

### Added

- feat: add MailHog (an email testing tool for developers)

### Changed

- refactor: improve puppet manifest
- chore: improve hiera example
- chore: use puppet-tajine 1.0.0
- docs: add some symfony debug command line


--------------------------------

## v2.0.1, 2023.05.10

### Changed

- Use Tajine v0.9.1


--------------------------------

## v2.0.0, 2023.04.20

### Changed

- Use Tajine v0.9.0

#### Breaking change

> You must manually clear the cache with the following command lines,
> before running the `vagrant up --provision` command:

```bash
vagrant ssh
sudo su
rm -rvf /opt/tajine/tajine/var/cache/prod/
```

--------------------------------

## v1.0.0, 2023.04.04

### Added

- add Vagrantfile
- add documentation
- use Puppet component module  `puppet-tajine` 0.3.0
- use Puppet component module  `puppet-tajine` 2.1.0


--------------------------------

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

### Fixed

### Security

--------------------------------

```
