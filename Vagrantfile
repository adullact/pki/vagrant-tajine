$script_ubuntu = <<-SCRIPT
if [ ! -f /opt/puppetlabs/bin/puppet ]; then
  sudo wget --quiet https://apt.puppetlabs.com/puppet7-release-jammy.deb # Ubuntu 22.04
  sudo dpkg -i puppet7-release-jammy.deb                                 # Ubuntu 22.04
  sudo apt-get update
  sudo apt-get install puppet-agent
fi
SCRIPT

Vagrant.configure("2") do |config|

  # Default values
  default_host_postgresql_port = 5432
  default_host_mailhog_port    = 8025
  default_host_cfssl_port      = 8888
  default_host_tajine_port     = 8002
  default_host_tajine_ip       = "127.0.0.1"

  # Environment variable customizations
  host_postgresql_port = ENV['VAGRANT_HOST_POSTGRESQL_PORT']   ? ENV['VAGRANT_HOST_POSTGRESQL_PORT']   : default_host_postgresql_port
  host_mailhog_port    = ENV['VAGRANT_HOST_MAILHOG_HTTP_PORT'] ? ENV['VAGRANT_HOST_MAILHOG_HTTP_PORT'] : default_host_mailhog_port
  host_cfssl_port      = ENV['VAGRANT_HOST_CFSSL_PORT']        ? ENV['VAGRANT_HOST_CFSSL_PORT']        : default_host_cfssl_port
  host_tajine_port     = ENV['VAGRANT_HOST_TAJINE_PORT']       ? ENV['VAGRANT_HOST_TAJINE_PORT']       : default_host_tajine_port
  host_tajine_ip       = ENV['VAGRANT_HOST_TAJINE_IP']         ? ENV['VAGRANT_HOST_TAJINE_IP']         : default_host_tajine_ip

  config.vm.box = "ubuntu/jammy64" # Ubuntu 22.04
  config.vm.hostname = "pki.example.org"
  config.vm.network "forwarded_port", id: 'Tajine',     guest: 80,   host: host_tajine_port,     auto_correct: true, host_ip: host_tajine_ip
  config.vm.network "forwarded_port", id: 'CfsslApi',   guest: 8888, host: host_cfssl_port,      auto_correct: true, host_ip: host_tajine_ip
  config.vm.network "forwarded_port", id: 'MailHog',    guest: 8025, host: host_mailhog_port,    auto_correct: true, host_ip: host_tajine_ip
  config.vm.network "forwarded_port", id: 'PostgreSQL', guest: 5432, host: host_postgresql_port, auto_correct: true, host_ip: host_tajine_ip
  config.vm.provider "virtualbox" do |vb|
    vb.name = "DEMO_TAJINE"
  # vb.memory = "4096"
  # vb.cpus = "4"
  end

  config.vm.synced_folder "puppet/hieradata/", "/tmp/vagrant-puppet/hieradata"
  config.vm.provision "shell", inline: $script_ubuntu
  config.vm.provision "puppet" do |puppet|
    # documentation
    # https://developer.hashicorp.com/vagrant/docs/provisioning/puppet_apply
    puppet.hiera_config_path = "puppet/hiera.yaml"
    puppet.working_directory = "/tmp/vagrant-puppet"
    puppet.module_path = "puppet/modules"
    puppet.manifest_file = "default.pp"
    puppet.manifests_path= "puppet/manifests"
    # puppet.options = "--debug"
  end

  # Shared folders: Host <--> Guest (VM)
  ###############################################################################################
  ###############################################################################################

  # Default share folder:  Host  "./"  <--> Guest (VM) "/vagrant"
  #
  # Share an additional folder to the guest VM.
  # - The first argument is the path on the host to the actual folder.
  # - The second argument is the path on the guest to mount the folder.
  # - And the optional third argument is a set of non-required options.
  config.vm.synced_folder "./VM_shared/webapp",         "/opt/tajine/",         create: true, owner: "www-data", group: "www-data"
  config.vm.synced_folder "./VM_shared/webapp_config",  "/etc/tajine/",         create: true, owner: "www-data", group: "www-data"
  config.vm.synced_folder "./VM_shared/webapp_log",     "/var/tajine/log/",     create: true, owner: "www-data", group: "www-data"
  config.vm.synced_folder "./VM_shared/webapp_session", "/var/tajine/sessions", create: true, owner: "www-data", group: "www-data"

  # Message to show after vagrant up
  config.vm.post_up_message = <<-MESSAGE
  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️

  ⚠️   No security has been made for production.        ⚠️
  --------------------------------------------------------


  CFSSL API       https://github.com/cloudflare/cfssl/blob/master/doc/
  --------------------------------------------------------------------
  /api/v1/cfssl/crl       Generates a CRL out of the certificate DB
  /api/v1/cfssl/info      Get information about certification authority (CA)
  /api/v1/cfssl/newcert   Generate a new private key and certificate
  /api/v1/cfssl/certinfo  Lookup a certificate's info
  /api/v1/cfssl/revoke    Revoke a certificate

  # Example: /api/v1/cfssl/info
  curl -s -d '{}' -H "Content-Type: application/json" -X POST #{host_tajine_ip}:#{host_cfssl_port}/api/v1/cfssl/info


  Tajine software
  --------------------------------------------------------------------

  # Display application logs
  vagrant ssh -c 'sudo -i tail -f /var/tajine/log/prod-$(date '+%Y-%m-%d').log'

  # Lists all dotenv files with variables and values
  vagrant ssh -c 'sudo -i /opt/tajine/tajine/bin/console debug:dotenv'

  # Lists all application routes (URL, HTTP method, ...)
  vagrant ssh -c 'sudo -i /opt/tajine/tajine/bin/console debug:router'

  --------------------------------------------------------------------
  Use 'vagrant port' command line to see port mapping.

  Default :  5432 (guest) --> #{host_postgresql_port} (host)  PostgreSQL   port
             8888 (guest) --> #{host_cfssl_port} (host)  CFSSL   API  port
             8025 (guest) --> #{host_mailhog_port} (host)  MailHog HTTP port
               80 (guest) --> #{host_tajine_port} (host)  Tajine  HTTP port

  --------------------------------------------------------------------
  CFSSL API .......... http://#{host_tajine_ip}:#{host_cfssl_port}/api/v1/cfssl/<endpoint>
  Webmail ............ http://#{host_tajine_ip}:#{host_mailhog_port}
  Tajine webapp ...... http://#{host_tajine_ip}:#{host_tajine_port}
  --------------------------------------------------------------------
  Tajine Account ..... admin-tajine@example.org
  Tajine Password .... admin-tajine_PaSsWord_IsNotSoSecretChangeIt
  --------------------------------------------------------------------

  --------------------------------------------------------
  ⚠️   Created only for test or demonstration purposes. ⚠️
  --------------------------------------------------------
  MESSAGE
end
