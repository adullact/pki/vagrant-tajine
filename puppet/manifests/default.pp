# Timezone
##########################################################################
# TAJINE software configures the time zone
# used in web application (by default, it is Europe/Paris),
# independently of the time zone used by the operating system.

# Operating system use Europe/Paris timezone
exec { 'timedatectl set-timezone':
    command => "/usr/bin/timedatectl set-timezone Europe/Paris",
    unless  => "/usr/bin/timedatectl | grep Europe/Paris",
}
    ##########################################################################
    #     # Operating system use America/New_York timezone
    #     exec { 'timedatectl set-timezone':
    #         command => "/usr/bin/timedatectl set-timezone America/New_York",
    #         unless  => "/usr/bin/timedatectl | grep America/New_York",
    #     }
    ##########################################################################

# Prerequired Puppet component modules
##########################################################################
include ::apt
include cfssl
include mailhog
# include apache
# include tajine

# Tajine prerequired packages
##########################################################################
$preRequistes = [
    'php8.1-bcmath',
    'php8.1-curl',
    'php8.1-pgsql',
    'php8.1-mbstring',
    'php8.1-intl',
    'php8.1-xml',
]
package { $preRequistes:
    ensure  => 'installed',
    require => Class['apt::update'],
}

# Tajine software
##########################################################################
class { 'tajine':
    require => [
        Class['Postgresql::Server'],
        Class['apache'],
        Class['apache::mod::php'],
    ],
}

# exec { 'disable HTTPS Tajine configuration':
#     command => "/bin/echo 'nelmio_security:\n    forced_ssl:\n        enabled: false' >  /opt/tajine/tajine/config/packages/prod/nelmio_security.yaml && /opt/tajine/tajine/bin/console cache:clear",
#     unless  => "/bin/cat /opt/tajine/tajine/config/packages/prod/nelmio_security.yaml | grep 'nelmio_security:\n    forced_ssl:\n        enabled: false'",
#     require => [
#         Class['tajine'],
#     ],
# }

# Enabled Tajine DEBUG_CERT_BY_ADMIN env
#     exec { 'tajine DEBUG_CERT_BY_ADMIN  enabled':
#         command => "/bin/echo 'DEBUG_CERT_BY_ADMIN=enabled' >>  /opt/tajine/tajine/.env.local",
#         unless  => "/bin/cat /opt/tajine/tajine/.env.local | grep 'DEBUG_CERT_BY_ADMIN=enabled'",
#         require => [
#             Class['tajine'],
#         ],
#     }

# Configure Apache server and vhost
##########################################################################
class { 'apache':
    default_vhost    => false,
    mpm_module       => 'prefork',
    default_mods    => [
        'mime',     # cfssl crl
        'php',      # tajine
        'rewrite',  # tajine
        'headers'   # tajine (required to add cache headers and security headers)
    ]
}

apache::vhost { 'pki.example.com':
    port    => 80,
    docroot => '/opt/tajine/tajine/public',
    docroot_owner => 'www-data',
    docroot_group => 'www-data',
    directories   => [
        {
            path           => '/opt/tajine/tajine/public',
            allow_override => 'All',
        },
    ],
}
